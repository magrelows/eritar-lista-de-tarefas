//
//  LocalNotificationUtils.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 12/03/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//


import Foundation
import CoreData
import UserNotifications

public class LocalNotificationUtils {

    func createLocalNotification(data: Date, mensagem: String) {
        
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = "ERITAR"
        content.subtitle = "Lista de Tarefas"
        content.body = mensagem
        
        //getting the notification trigger
        //it will be called after 5 seconds
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: data)
        
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 20, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        let identifier = UUID()
        //getting the notification request
        let request = UNNotificationRequest(identifier: identifier.uuidString, content: content, trigger: trigger)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    /// Method responsible to clean all notification
    func removeAll() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
}
