//
//  TarefasRepository.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 01/03/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class TarefasRepository {
    let key = "Tarefas"
    
    public func salvarRepositorio( tarefa: TarefasModel ) -> TarefasModel? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: key, in: context)
        let novaTarefa = NSManagedObject(entity: entity!, insertInto: context)
        
        novaTarefa.setValue(tarefa.lembrete, forKey: "lembrete")
        novaTarefa.setValue(tarefa.dataFinal, forKey: "dataFinal")
        novaTarefa.setValue(tarefa.detalhe, forKey: "detalhe")
        
        do {
            try context.save()
            tarefa.tarefa = novaTarefa
            print("salvou com sucesso a tarefa")
            return tarefa
        } catch {
            print("falha ao salvar tarefa")
            return nil
        }
    }
    
    
    public func removerTarefaRepositorio( tarefa: TarefasModel ) -> Bool? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        do {
            try context.delete(tarefa.tarefa!)
            return true
        } catch {
            return false
        }
    }
    
    
    
    
    public func retornarTodas () -> [TarefasModel]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: key)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            let todasTarefas = converteNSObjectParaTarefas(objects: result as! [NSManagedObject])
            return todasTarefas
        } catch {
            print("Failed")
        }
        
        return nil
    }
    
    
    public func retornarPorDataAscendenteEMaiorQueHoje() -> [TarefasModel]? {
        
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.hour = -2
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: key)
        request.sortDescriptors = [NSSortDescriptor(key: "dataFinal", ascending: true)]
        
        let predicate = NSPredicate(format: "dataFinal > %@", futureDate! as NSDate)
        request.predicate = predicate
        do {
            let result = try context.fetch(request)
            let todasTarefas = converteNSObjectParaTarefas(objects: result as! [NSManagedObject])
            return todasTarefas
        } catch {
            print("Failed")
        }
        
        return nil
    }
    
    private func converteNSObjectParaTarefas( objects : [NSManagedObject]) -> [TarefasModel] {
        var tarefasLista: [TarefasModel] = []
        
        for data in objects {
            let tarefaAux = TarefasModel()
            tarefaAux.detalhe = data.value(forKey: "detalhe") as? String
            tarefaAux.dataFinal = data.value(forKey: "dataFinal") as? Date
            tarefaAux.lembrete = data.value(forKey: "lembrete") as? Date
            tarefaAux.tarefa = data
            
            tarefasLista.append(tarefaAux)
        }
        
        return tarefasLista
    }
}
