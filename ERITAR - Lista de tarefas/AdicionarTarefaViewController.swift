//
//  AdicionarTarefaViewController.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 01/03/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit
import GoogleMobileAds
import UserNotifications

class AdicionarTarefaViewController: UIViewController{

    var bannerView: GADBannerView!
    
    @IBOutlet weak var descricaoText: UITextField!
    @IBOutlet weak var dataFinalPicker: UIDatePicker!
    @IBOutlet weak var lembretePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // In this case, we instantiate the banner with desired ad size.
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-5493383814534386/4093572691"
        bannerView.rootViewController = self
        
        addBannerViewToView(bannerView)
        bannerView.load(GADRequest())
        
        
        UNUserNotificationCenter.current().delegate = self
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    @IBAction func criarTarefaClick(_ sender: Any) {
        
        if descricaoText.text == "" {
            // create the alert
            let alert = UIAlertController(title: "Erro",
                                          message: "Insira um Descrição a sua Tarefa.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            return
        }

        if dataFinalPicker.date < Date() {
            // create the alert
            let alert = UIAlertController(title: "Erro",
                                          message: "Insira uma Data de Tarefa superior a agora.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if lembretePicker.date < Date() {
            // create the alert
            let alert = UIAlertController(title: "Erro",
                                          message: "Insira um Lembrete com uma Data superior a agora.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        var tarefaTeste = TarefasModel()
        tarefaTeste.dataFinal = dataFinalPicker.date
        tarefaTeste.lembrete = lembretePicker.date
        tarefaTeste.detalhe = descricaoText.text
        
        tarefaTeste = tarefaTeste.criarNovaTarefa(tarefaNova: tarefaTeste)!
        
        createLocalNotification(data: tarefaTeste.lembrete!, mensagem: "Lembrete: \( tarefaTeste.detalhe!)")
        createLocalNotification(data: tarefaTeste.dataFinal!, mensagem: "Fim: \( tarefaTeste.detalhe!)")
        
       self.navigationController?.popToRootViewController(animated: true)
    }
    
    func createLocalNotification(data: Date, mensagem: String) {
        
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = "ERITAR"
        content.subtitle = "Lista de Tarefas"
        content.body = mensagem
        
        //getting the notification trigger
        //it will be called after 5 seconds
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: data)

        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 20, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)

        let identifier = UUID()
        //getting the notification request
        let request = UNNotificationRequest(identifier: identifier.uuidString, content: content, trigger: trigger)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    

    
    /*func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AdicionarTarefaViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        // some other way of handling notification
        completionHandler([.alert, .sound])
    }
   
}
