//
//  ViewController.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 28/02/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit
import GoogleMobileAds
import UserNotifications

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var bannerView: GADBannerView!
    var tarefas: [TarefasModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // In this case, we instantiate the banner with desired ad size.
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-5493383814534386/4093572691"
        bannerView.rootViewController = self
        
        addBannerViewToView(bannerView)
        bannerView.load(GADRequest())
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        tarefas = TarefasModel().retornarPorDataAscendenteEMaiorQueHoje()!
        tableView.reloadData()
        
        if tarefas.count == 0 {
            let alert = UIAlertController(title: "Sem Tarefas",
                                          message: "Olá aparenta estar sem tarefas. Gostaria de criar uma?",
                                          preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "SIM"
                , style: UIAlertAction.Style.default
                , handler: { (UIAlertAction) in
                    self.performSegue(withIdentifier: "criarTarefaSegue", sender: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    
    
    
    
    //Table View delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarefas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tarefasCell", for: indexPath) as! TarefasTableViewCell
        
        let dateFormmater = DateFormatter()
        
        dateFormmater.timeZone = TimeZone(identifier: TimeZone.current.abbreviation() ?? "")
        dateFormmater.dateFormat = "dd/MM/yyyy HH:mm"
        
        if tarefas[ indexPath.row ].dataFinal! > Date() && tarefas[ indexPath.row ].lembrete! < Date(){
            cell.backgroundColor = UIColor.yellow
        }else if tarefas[ indexPath.row ].dataFinal! < Date() {
            cell.backgroundColor = UIColor.red
        }
        
        cell.dataFinal.text = dateFormmater.string(from: tarefas[ indexPath.row ].dataFinal!)
        cell.lembrete.text = dateFormmater.string(from: tarefas[ indexPath.row ].lembrete!)
        cell.detalhe.text = tarefas[ indexPath.row ].detalhe
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            TarefasModel().removerTarefa(tarefaNova: tarefas[indexPath.row])
            tarefas = TarefasModel().retornarPorDataAscendenteEMaiorQueHoje()!
            tableView.reloadData()
            
            LocalNotificationUtils().removeAll()
            for tarefaAux in tarefas {
                
                LocalNotificationUtils().createLocalNotification(data: tarefaAux.lembrete!, mensagem: "Lembrete: \( tarefaAux.detalhe!)")
                LocalNotificationUtils().createLocalNotification(data: tarefaAux.dataFinal!, mensagem: "Fim: \( tarefaAux.detalhe!)")
            }
        }
    }
}

