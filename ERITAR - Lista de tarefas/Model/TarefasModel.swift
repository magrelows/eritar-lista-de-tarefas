//
//  TarefasModel.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 01/03/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import Foundation
import CoreData

public class TarefasModel {
    var tarefa: NSManagedObject?
    var detalhe: String?
    var dataFinal: Date?
    var lembrete: Date?
    
    public func criarNovaTarefa( tarefaNova: TarefasModel ) -> TarefasModel? {
        return TarefasRepository().salvarRepositorio(tarefa: tarefaNova)
    }
    
    public func removerTarefa( tarefaNova: TarefasModel ) -> Bool? {
        if tarefaNova.tarefa != nil {
            return TarefasRepository().removerTarefaRepositorio(tarefa: tarefaNova)
        } else {
            return false
        }
    }
    
    
    public func retornaTodas () -> [TarefasModel]? {
        return TarefasRepository().retornarTodas()
    }
    
    public func retornarPorDataAscendenteEMaiorQueHoje () -> [TarefasModel]? {
        return TarefasRepository().retornarPorDataAscendenteEMaiorQueHoje()
    }
}
