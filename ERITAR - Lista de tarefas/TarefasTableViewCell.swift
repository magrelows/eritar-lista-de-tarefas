//
//  TarefasTableViewCell.swift
//  ERITAR - Lista de tarefas
//
//  Created by entelgy on 01/03/2019.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit

class TarefasTableViewCell: UITableViewCell {
    @IBOutlet weak var detalhe: UILabel!
    @IBOutlet weak var dataFinal: UILabel!
    @IBOutlet weak var lembrete: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
